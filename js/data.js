

const data = {
    "first_name": "Audrius",
    "last_name": "Adomavičius",
    "job_title": "frontend programuotojas",
    "photo": "images/foto.jpg",
    "phone": "tel: +37065087335",
    "email": "audriusad@gmail.com",
    "linkedin": "https://www.linkedin.com/in/audrius-a-859ab451",

    "skills": [
        "HTML",
        "CSS",
        "Javascript",
        "Wordpress",
    ],
    "education": [
        ["2020-2021", "SOFTWARE DEVELOPMENT ACADEMY" ],
        ["2019 08-09", "VILNIUS CODING SCHOOL"]
        ["2001-2005", "VILNIAUS DAILĖS AKADEMIJA"],
    ],
    "profile": "Man 47-eri. Esu pradedantysis programuotojas. Žengiu pirmuosius žingsnius šioje srityje. Labai viskas įdomu ir dinamiška. Iššūkių tikrai netrūksta, bet tai man patinka, tai yra vienas iš faktorių dėl ko ši sritis yra patraukli. ",

    "experience": [
        {
            "site_address": "http://rigra.lt",
            "description": "Tai mano pirmasis projektas: internetinis puslapis rigra.lt. Visas kodas parašytas nuo nulio, perkeltas į Wordpress sistemą, po to ir į nuotolinį serverį. Žinoma ne viskas šiame puslapyje tobula, bet informacija suvaikšto pakankamai sparčiai, viskas kas reikia tvarkingai atsidaro ir užsidaro."
        },
        {
            "site_address": "https://aadomavicius.github.io/Book-shop-project/",
            "description": 'Visada norėjau sukurti kažką tokio kaip elekroninę parduotuvę kur duomenys atkeliauja iš nuotolinio serverio(šiuo atveju duomenys ) ir "stebuklingu būdu" atsivaizduoja naršyklėje. Įdomu buvo, kad galėjau panaudoti OOP(objektinį programavimą. Šis projektėlis dar tesiamas..'
        },
  
    ]
    
};